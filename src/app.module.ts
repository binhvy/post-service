import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { AppController } from './app.controller';
import { AcceptLanguageResolver, I18nModule, QueryResolver } from 'nestjs-i18n';
import { join } from 'path';
import { TerminusModule } from '@nestjs/terminus';
import { APP_GUARD } from '@nestjs/core';
import { JwtAuthGuard, RolesGuard } from './core';
import { PostModule } from './modules/post/post.module';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { ConfigService } from '@nestjs/config';
import { PrismaService } from 'src/database/prisma.service';
import { PrismaModule } from './database/prisma.module';
import { ConfigModule } from '@nestjs/config';
import { config } from './config/config';
import { TracingMiddleware } from './core/middlewares/trace.middleware';
@Module({
  imports: [
    PostModule,
    ConfigModule.forRoot({
      isGlobal: true,
      load: [config],
    }),
    // JwtModule.registerAsync({
    //   imports: [ConfigModule],
    //   useFactory: async (configService: ConfigService) => ({
    //     secret: configService.get<string>('jwt.secret', 'yyy'),
    //     signOptions: {
    //       expiresIn: configService.get<string>('jwt.expire', 'yyy')
    //     },
    //   }),
    //   inject: [ConfigService],
    // }),
    // ClientsModule.registerAsync([
    //   {
    //     name: 'AUTH_SERVICE',
    //     imports: [ConfigModule],
    //     useFactory: async (configService: ConfigService) => ({
    //       transport: Transport.RMQ,
    //       options: {
    //         urls: [`${configService.get('rb_url')}`],
    //         queue: `${configService.get('auth_queue')}`,
    //         queueOptions: {
    //           durable: false,
    //         },
    //       },
    //     }),
    //     inject: [ConfigService],
    //   },
    // ]),
    TerminusModule,
  ],
  controllers: [AppController],
  providers: [
    {
      provide: APP_GUARD,
      useClass: JwtAuthGuard,
    },
    {
      provide: APP_GUARD,
      useClass: RolesGuard,
    },
  ],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(TracingMiddleware).forRoutes('*');
  }
}

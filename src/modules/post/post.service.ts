import { HttpException, HttpStatus, Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { Post } from '@prisma/client';
import { firstValueFrom } from 'rxjs';
import { CreatePostDto, UpdatePostDto } from '../../core';
import { GetResponse } from '../../types';
import { RedisService } from 'src/redis/redis.service';
import { PrismaService } from 'src/database/prisma.service';
import * as apm from 'elastic-apm-node';
@Injectable()
export class PostService {
  constructor(
    @Inject('AUTH_SERVICE') private readonly authClient: ClientProxy,
    private readonly prisma: PrismaService,
    private readonly redisService: RedisService,
  ) {
    this.authClient.connect();
  }

  public async getOnePost(id: any) {
    const post = await this.prisma.post.findUnique({
      where: {
        id,
      },
    });
    if (!post) {
      throw new HttpException('post_not_found', HttpStatus.NOT_FOUND);
    }
    // const createdBy = await firstValueFrom(
    //   this.authClient.send(
    //     'get_user_by_id',
    //     JSON.stringify({ id: post.author }),
    //   ),
    // );
    // post.author = createdBy;
    return post;
  }

  public async createNewPost(
    data: CreatePostDto,
    userId: number,
  ): Promise<Post> {
    try {
      const post = {} as Post;
      post.content = data.content;
      post.author = userId;
      post.title = data.title;
      post.image = data.fileId;
      const create_post = await this.prisma.post.create({ data: post });
      // const createdBy = await firstValueFrom(
      //   this.authClient.send(
      //     'get_user_by_id',
      //     JSON.stringify({
      //       id: userId,
      //     }),
      //   ),
      // );
      // create_post.author = createdBy;
      return create_post;
    } catch (e) {
      throw e;
    }
  }

  public async getAllPosts(data: {
    page: number;
    limit: number;
    term: string;
  }): Promise<any> {
    const span = apm.currentTransaction?.startSpan('get-all-post');
    // Logic để lấy dữ liệu
    let { limit, page } = data;
    if (!page || page === 0) {
      page = 1;
    }
    if (!limit) {
      limit = 10;
    }
    const skip = (page - 1) * limit;
    try {
      // await this.redisService.set('ok', 'hii', 10000);
      // return await this.redisService.get('ok');
      // let rs = await this.prisma.post.findFirst()

      const createdBy = await firstValueFrom(
        this.authClient.send(
          'get_user_by_id_in_controller',
          {
            headers: { 'x-elastic-apm-traceparent': apm.currentTraceparent },
          }),
      );
      console.log('createdBy', createdBy);
    } catch (error) {
      console.log('eee', error);
    }
    span?.end();

    return 1;
  }

  public async updatePost(id: number, data: UpdatePostDto): Promise<Post> {
    try {
      const findPost = await this.prisma.post.findUnique({ where: { id } });
      if (!findPost) {
        throw new HttpException('post_not_found', HttpStatus.NOT_FOUND);
      }
      const post = await this.prisma.post.update({
        where: {
          id,
        },
        data: {
          title: data.title,
          content: data.content,
        },
      });
      // const createdBy = await firstValueFrom(
      //   this.authClient.send(
      //     'get_user_by_id',
      //     JSON.stringify({ id: post.author }),
      //   ),
      // );
      // post.author = createdBy;
      return post;
    } catch (e) {
      throw e;
    }
  }
}

import {
  ClassSerializerInterceptor,
  Logger,
  ValidationPipe,
} from '@nestjs/common';
import { NestFactory, Reflector } from '@nestjs/core';
import { Transport } from '@nestjs/microservices';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { HttpExceptionFilter, ResponseInterceptor } from './core/interceptor';
import { ExpressAdapter } from '@nestjs/platform-express';
import * as express from 'express';
import helmet from 'helmet';
import { ConfigService } from '@nestjs/config';
import * as apm from 'elastic-apm-node';

function configureSwagger(app): void {
  const config = new DocumentBuilder()
    .setTitle('post-service')
    .setVersion('1.0')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('/api/docs', app, document);
}

async function bootstrap() {
  const logger = new Logger();
  const app = await NestFactory.create(
    AppModule,
    new ExpressAdapter(express()),
    {
      bufferLogs: true,
      cors: true,
    },
  );
  app.setGlobalPrefix('/api');
  app.use(helmet());
  const configService = app.get(ConfigService);
  const nodeEnv = configService.get<string>('node_env');
  if (!nodeEnv) return 1;
  console.log(`Ứng dụng đang chạy trong môi trường: ${nodeEnv}`);
  const moduleRef = app.select(AppModule);
  const reflector = moduleRef.get(Reflector);
  app.useGlobalInterceptors(
    new ResponseInterceptor(reflector),
    new ClassSerializerInterceptor(reflector),
  );
  
  apm.start({
    serviceName: 'post_service',
    secretToken: 'X2vkRYpPDZdKCdo6DZ',
    serverUrl: 'https://c866e20e1d2d42d3974799af0e351965.apm.us-east-2.aws.elastic-cloud.com:443',
    environment: 'my-environment'
  })
  app.useGlobalFilters(new HttpExceptionFilter());
  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
      forbidNonWhitelisted: true,
      transform: true,
    }),
  );
  configureSwagger(app);
  app.connectMicroservice({
    transport: Transport.RMQ,
    options: {
      urls: [`${configService.get('rabbitmq.url')}`],
      queue: `${configService.get('rabbitmq.service.post')}`,
      queueOptions: { durable: false },
      prefetchCount: 1,
    },
  });
  await app.startAllMicroservices();
  await app.listen(configService.get('PORT'));
  logger.log(
    `🚀 Post service started successfully on port ${configService.get('PORT')}`,
  );
}
bootstrap();
